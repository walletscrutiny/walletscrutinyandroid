This app is not meant for a mass market but is crucial for
[WalletScrutiny](https://walletscrutiny.com) to work.

Google allows developers to target languages, countries, devices, ... with
individual versions of their app and there is no public api giving access to all
those variations.

Additionally Google might distribute altered versions of the app without the
provider's knowledge, thanks to App Bundles, Google is pushing for recently: A
scheme under which the developer shares the app signing key with Google.

In order to capture all versions of an app that are being distributed, this open
source software is designed to:

1. Monitor app install/update events
1. Check if the appId is of interest
1. Hash the new APK
1. Check with a server if the hash is known
1. If the hash is unknown, upload the APK to a server
1. The server unpacks the received APK
1. Determines the appId and version
1. Tries to reproduce the build
1. Publicly shares results
1. Should reproducing the build fail, the appId could be automatically flagged as "under
   investigation", with app users getting notified not to update that app until the issue is
   resolved.

Build Instructions
==================

```
$ ./gradlew :app:assemble
$ ls app/build/outputs/apk/release/*.apk
app/build/outputs/apk/release/app-release-unsigned.apk
```

Running server in development
=============================

**Using docker:**

1. Set `/ws/server/var/lib/walletscrutiny` as baseFolder, `apksigner` as apksigner path and `/ws-com/test.sh` as testScript in `context-dev.xml`.
1. Install docker and docker-compose then run server using `docker-compose up`.
1. Change the baseUrl in UpdateCheckWorker.kt to point to `http://localhost:8081/`. (TODO: Use some configuration settings for this)

**Local deployment:**

1. You need to have `apktool`, `apksigner`, `disorderfs` (for MyCelium), `fuse-overlayfs` and `podman` installed.
1. In some Debian based distros you may need to set `unqualified-search-registries=["docker.io"]` in `/etc/containers/registries.conf`.
1. Clone **Waletscrutiny** project to let server run test scripts.
1. Set your proper local configurations in `src/main/webapp/META-INF/context-dev.xml`.
1. Run the server with `./gradlew appRunWar`.
1. Change the baseUrl in UpdateCheckWorker.kt to point to `http://localhost:8081/`. (TODO: Use some configuration settings for this)

Building server in production
=============================

1. Build the war with `./gradlew :server:war -Pprod`.
2. Upload war file to tomcat server.

If you are running server as a system user like tomcat8, you should take additional steps:
1. Enable lingering for tomcat8 user: `loginctl enable-linger tomcat8`.
2. Edit `/etc/subuid` and `/etc/subgid` and add a record for tomcat8 with 65536 sub ids.
3. (Optional) If you want to test scripts as tomcat8 user, you can enable bash for tomcat8 user in `/etc/passwd` by replacing `/bin/false` with `/bin/bash`.
