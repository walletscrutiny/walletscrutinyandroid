# The main image is enough to run the server (with apktool and apksigner installed)
# But to run test scripts we need some modifications
FROM walletscrutiny/android:5

# Install dependencies
RUN apt-get update && \
  apt-get install -y gnupg2 docker.io disorderfs && \
  sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list" && \
  wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_20.04/Release.key -O- | apt-key add - && \
  apt-get update && \
  DEBIAN_FRONTEND="noninteractive" apt-get install -y podman && \
  rm -rf /var/lib/apt/lists/* && \
  apt-get autoremove -y && \
  apt-get clean

RUN echo "[engine]\ncgroup_manager = \"cgroupfs\"" >> /etc/containers/containers.conf

# Clone walletScrutinyCom to get test scripts
RUN git clone https://gitlab.com/walletscrutiny/walletScrutinyCom.git /ws-com

# Create app directory
RUN mkdir -p /ws
WORKDIR /ws

EXPOSE 8080

ENTRYPOINT [ "./entrypoint.sh" ]
