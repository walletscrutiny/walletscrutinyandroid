package com.walletscrutiny.server

import com.google.gson.GsonBuilder
import com.walletscrutiny.libdataexchange.WSApplicationState
import java.io.File
import java.util.*
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet("/admin")
class AdminController : HttpServlet() {
    private val gson = GsonBuilder().disableHtmlEscaping().setDateFormat("yyyy-MM-dd hh:mm:ss").create()
    private lateinit var appsFolder: File
    private lateinit var notReproducibleFolder: File

    override fun init() {
        val baseFolder = File(servletContext.getInitParameter("baseFolder"))
        val dataFolder = File("$baseFolder/dat")
        appsFolder = File("$dataFolder/apps")
        notReproducibleFolder = File("$dataFolder/not-reproducible")
    }

    /**
     * Accepts an action keyword to perform and an api key
     */
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val action: String? = req.getParameter("action")
        val apiKey: String? = req.getParameter("apiKey")

        when (action) {
             // This actions checks the api key access which is used to show appropriate actions in developers panel
            "apiKeyAccess" -> {
                res.writer.write(gson.toJson(mapOf(
                    "isAdmin" to StateHelper.getAPIKeyAccess(apiKey)
                )))
                return
            }
        }
    }

    /**
     * Accepts an action keyword to perform and an api key
     */
    override fun doPut(req: HttpServletRequest, resp: HttpServletResponse) {
        val action: String? = req.getParameter("action")
        val apiKey: String? = req.getParameter("apiKey")

        if (!StateHelper.getAPIKeyAccess(apiKey)) {
            resp.status = HttpServletResponse.SC_UNAUTHORIZED
            return
        }

        when (action) {
            // This actions marks a not-reproducible apk as reproducible
            "markAsReproducible" -> {
                val hash: String? = req.getParameter("appHash")
                if (hash.isNullOrBlank()) {
                    resp.status = HttpServletResponse.SC_BAD_REQUEST
                    return
                }

                val appId = StateHelper.getAppIdForHashInPublicState(hash)
                if (appId == null) {
                    resp.status = HttpServletResponse.SC_NOT_FOUND
                    return
                }

                val oldState = StateHelper.getState().apps[appId]!!.knownBinaries[hash]!!
                StateHelper.saveState(appId, hash, null, WSApplicationState(
                    Date(),
                    oldState.versionName,
                    oldState.versionCode,
                    oldState.reporterId,
                    true,
                    oldState.signatureVerified,
                    oldState.testResult
                ))

                val apkFile = File("$notReproducibleFolder").listFiles()!!
                    .find { file -> file.name.endsWith("$hash.apk") }
                if (apkFile != null) {
                    val destinationFile = File("$appsFolder/${apkFile.name}")
                    if (destinationFile.isFile) {
                        destinationFile.delete()
                    }
                    apkFile.renameTo(destinationFile)
                }

                resp.writer.println(
                    gson.toJson(
                        mapOf(
                            "success" to "APK Marked as reproducible",
                            "hash" to hash
                        )
                    )
                )
            }
        }
    }
}
