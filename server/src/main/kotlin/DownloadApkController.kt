package com.walletscrutiny.server

import java.io.*
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet("/download-apk")
class DownloadApkController : HttpServlet() {
    private lateinit var appsFolder: File
    private lateinit var notReproducibleFolder: File

    override fun init() {
        val baseFolder = File(servletContext.getInitParameter("baseFolder"))
        val dataFolder = File("$baseFolder/dat")
        appsFolder = File("$dataFolder/apps")
        notReproducibleFolder = File("$dataFolder/not-reproducible")
    }

    /**
     * Accepts an appHash and an optional apiKey.
     * This API queries a previously uploaded apk file with provided appHash.
     * If an apiKey was provided, The method tries to find and return
     * the file uploaded by the same api key.
     */
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val hash = req.getParameter("appHash")
        val apiKey: String? = req.getParameter("apiKey")
        if (!apiKey.isNullOrBlank() && !StateHelper.checkAPIKeyValidity(apiKey)) {
            res.status = HttpServletResponse.SC_UNAUTHORIZED
            return
        }
        val subFolder = if (apiKey.isNullOrBlank()) "" else "/$apiKey"

        val downloadableFile = File("$notReproducibleFolder$subFolder").listFiles()!!
            .plus(File("$appsFolder$subFolder").listFiles()!!)
            .find { file -> file.name.endsWith("$hash.apk") }

        if (downloadableFile == null) {
            res.status = HttpServletResponse.SC_NOT_FOUND
            return
        }
        val inStream = FileInputStream(downloadableFile)

        val context = servletContext
        var mimeType = context.getMimeType(downloadableFile.canonicalPath)
        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream"
        }

        res.contentType = mimeType
        res.setContentLength(downloadableFile.length().toInt())

        // forces download
        val headerKey = "Content-Disposition"
        val headerValue = java.lang.String.format("attachment; filename=\"%s\"", downloadableFile.name)
        res.setHeader(headerKey, headerValue)

        val outStream: OutputStream = res.outputStream

        val buffer = ByteArray(4096)
        var bytesRead: Int

        while (inStream.read(buffer).also { bytesRead = it } != -1) {
            outStream.write(buffer, 0, bytesRead)
        }

        inStream.close()
        outStream.close()
    }
}
