package com.walletscrutiny.server

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlFactory
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.gson.GsonBuilder
import com.walletscrutiny.libdataexchange.WSApplicationState
import com.walletscrutiny.libdataexchange.WSError
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.yield
import org.slf4j.LoggerFactory
import java.io.*
import java.net.URL
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.annotation.MultipartConfig
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.concurrent.thread

@WebServlet("/apks")
@MultipartConfig(
    fileSizeThreshold = 2_000_000,
    maxFileSize = 150_000_000,
    maxRequestSize = 155_000_000
)
class ApksController : HttpServlet() {
    private val yamlMapper = ObjectMapper(YAMLFactory()).also { it.registerKotlinModule() }
    private val xmlMapper = ObjectMapper(XmlFactory()).also { it.registerKotlinModule() }
    private lateinit var baseFolder: File
    private lateinit var dataFolder: File
    private lateinit var appsFolder: File
    private lateinit var uploadsFolder: File
    private lateinit var notReproducibleFolder: File
    private lateinit var apktoolFrameworkPath: File
    private var ip = "127.0.0.1"
    private val logger = LoggerFactory.getLogger(ApksController::class.java)
    private val startTime = Date()
    private val gson = GsonBuilder().disableHtmlEscaping().setDateFormat("yyyy-MM-dd hh:mm:ss").create()
    private val hashesInProcess = mutableMapOf<String, Long>()

    override fun init() {
        baseFolder = File(servletContext.getInitParameter("baseFolder"))
        dataFolder = File("$baseFolder/dat")
        appsFolder = File("$dataFolder/apps")
        uploadsFolder = File("$dataFolder/uploads")
        notReproducibleFolder = File("$dataFolder/not-reproducible")
        apktoolFrameworkPath = File("$dataFolder/framework")

        detectIP()
        StateHelper.loadState(dataFolder)
        createFolders()
    }

    private fun createFolders() {
        listOf(dataFolder, appsFolder, uploadsFolder, notReproducibleFolder, apktoolFrameworkPath).forEach {
            createFolder(it)
        }

        listOf(appsFolder, uploadsFolder, notReproducibleFolder).forEach { parent ->
            StateHelper.getAPIKeys().forEach {
                createFolder(File(parent, it))
            }
        }
    }

    private fun createFolder(file: File) {
        if (!file.isDirectory && !file.mkdirs()) {
            throw Error(
                """Can't create folder ${file.name}
                   Please allow the tomcat user to write to $baseFolder by creating a file like
                   $ cat /etc/systemd/system/tomcat9.service.d/walletscrutiny.conf
                   [Service]
                   ReadWritePaths=/var/lib/walletscrutiny/
                   and create that folder structure
                """.trimIndent()
            )
        }
    }

    private fun detectIP() {
        // this is only for convenience. Should it ever be a critical part, add checks and fallbacks
        ip = try {
            BufferedReader(InputStreamReader(URL("http://checkip.amazonaws.com").openStream()))
                .readLine()
        } catch (e: Exception) {
            "127.0.0.1"
        }
    }

    /**
     * If appId and appHash is provided, returns the known state of the provided appHash.
     * If appId and versionCode is provided, returns the available update info for the appId.
     * If appId is provided, returns all known states for the appId.
     * Otherwise returns an explanation.
     */
    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val appId = req.getParameter("appId")
        val hash = req.getParameter("appHash")
        val versionCode = req.getParameter("versionCode")?.toLong()
        res.setHeader("content-type", APP_JSON)
        val apiKey: String? = req.getParameter("apiKey")
        if (!apiKey.isNullOrBlank() && !StateHelper.checkAPIKeyValidity(apiKey)) {
            res.status = HttpServletResponse.SC_UNAUTHORIZED
            return
        }
        // TODO: Mohammad 7/27/21 - Use a url parameter like "action" to perform different actions instead of relying on the sent data
        when {
            appId != null && hash != null -> getKnownState(res, appId, hash, apiKey)
            appId != null && versionCode != null -> checkForUpdates(res, appId, versionCode)
            appId != null -> getKnownStates(res, appId, apiKey)
            else -> res.writer.write(explain())
        }
    }

    /**
     * Returns the state for a specific {appId} and apk {hash}
     */
    private fun getKnownState(
        res: HttpServletResponse,
        appId: String,
        hash: String,
        apiKey: String?
    ) {
        val appState = StateHelper.getState(apiKey).apps[appId]?.knownBinaries?.get(hash)
        val success = appState != null
        res.writer.write(
            if (success) {
                gson.toJson(appState)
            } else {
                res.status = HttpServletResponse.SC_NOT_FOUND
                gson.toJson(NO_DATA_ERROR)
            }
        )
    }

    /**
     * Returns all states for a specific {appId}
     */
    private fun getKnownStates(res: HttpServletResponse, appId: String, apiKey: String?) {
        val binaries = StateHelper.getState(apiKey).apps[appId]?.knownBinaries
        if (binaries?.size ?: 0 == 0) {
            res.status = HttpServletResponse.SC_NOT_FOUND
        }
        res.writer.write(
            gson.toJson(binaries ?: NO_DATA_ERROR)
        )
    }

    /**
     * Returns states for a specific {appId} where the versionCodes
     * are bigger than the requested {versionCode}
     */
    private fun checkForUpdates(
            res: HttpServletResponse,
            appId: String,
            versionCode: Long
    ) {
        val appRecentVersions = StateHelper.getState().apps[appId]?.knownBinaries
                ?.filter { it.value.versionCode > versionCode }
                ?.map { it.value }
                ?.sortedBy { it.versionCode }
        val success = appRecentVersions != null
        res.writer.write(
                if (success) {
                    gson.toJson(appRecentVersions)
                } else {
                    res.status = HttpServletResponse.SC_NOT_FOUND
                    gson.toJson(NO_DATA_ERROR)
                }
        )
    }

    /**
     * API usage guide
     */
    private fun explain(): String {
        return gson.toJson(
            mapOf(
                "description" to "This service analyzes Android APKs",
                "knownApps" to StateHelper.getState().apps.keys,
                "upTime" to "${(System.currentTimeMillis() - startTime.time) / 1000}s",
                "IP" to ip
            )
        )
    }

    /**
     * Receives an apk to test.
     * If an apiKey is provided, the test would be done privately and won't affect public state.
     * If a revision is provided, the script tries to overwrite the tag to fetch from repository
     * with the provided commit.
     * Example:
     * curl --request POST --data-binary @'path/to/wallet.apk' \
     *  http://localhost:8080/server/apks?reporter=Leo&apiKey=3f83d7f985584ab4008b68f3badafbae&revision=e3c414ca
     */
    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        val apiKey: String? = req.getParameter("apiKey")
        val revision: String? = req.getParameter("revision")
        if ((!apiKey.isNullOrBlank() && !StateHelper.checkAPIKeyValidity(apiKey))
            || (apiKey.isNullOrBlank() && !revision.isNullOrBlank())) {
            resp.status = HttpServletResponse.SC_UNAUTHORIZED
            return
        }
        val subFolder = if (apiKey.isNullOrBlank()) "" else "/$apiKey"

        try {
            val hash = storeFileUpload(req, subFolder)
            val file = File("$uploadsFolder$subFolder/$hash.bin")
            if (apiKey.isNullOrBlank()) {
                checkPriorResult(file, hash)
            }
            // no exceptions so far
            val reporter = req.getParameter("reporter")
            resp.writer.println(
                gson.toJson(
                    mapOf(
                        "success" to "New file received.",
                        "hash" to hash,
                        "reporter" to reporter
                    )
                )
            )
            thread {
                try {
                    processNewFile(file, hash, reporter, revision, apiKey)
                } catch (e: FileRejectedException) {
                    // something was not right. Cleanup and keep supposed apk and message for analysis
                    logger.warn(e.message)
                    e.folder?.deleteRecursively()
                    e.file.delete()
                    hashesInProcess.remove(hash)
                }
            }
        } catch (e: FileRejectedException) {
            resp.status = HttpServletResponse.SC_BAD_REQUEST
            gson.toJson(
                WSError("We already know this APK.", e.message)
            )
        }
    }

    /**
     * This api is used to request re-evaluation of an apk.
     * If an apiKey is provided, the test would be done privately and won't affect public state.
     * If a revision is provided, the script tries to overwrite the tag to fetch from repository
     * with the provided commit.
     * Since an admin can request re-evaluation for both public and private results, the isPublic
     * specifies the type of the test (public or private). When isPublic is true, an admin apiKey
     * should be provided.
     * Example:
     * curl --request PUT \
     *  http://localhost:8080/server/apks?appHash=cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c&reporter=Leo&apiKey=3f83d7f985584ab4008b68f3badafbae&revision=e3c414ca&isPublic=true
     */
    override fun doPut(req: HttpServletRequest, resp: HttpServletResponse) {
        val hash: String? = req.getParameter("appHash")
        val apiKey: String? = req.getParameter("apiKey")
        val revision: String? = req.getParameter("revision")
        val isPublic: Boolean = req.getParameter("isPublic") == "true"
        if (apiKey.isNullOrBlank() || !StateHelper.checkAPIKeyValidity(apiKey)
            || (isPublic && !StateHelper.getAPIKeyAccess(apiKey))) {
            resp.status = HttpServletResponse.SC_UNAUTHORIZED
            return
        }
        if (hash.isNullOrBlank()) {
            resp.status = HttpServletResponse.SC_BAD_REQUEST
            return
        }

        val subFolder = if (isPublic) "" else "/$apiKey"
        val apkFile = File("$notReproducibleFolder$subFolder").listFiles()!!
            .find { file -> file.name.endsWith("$hash.apk") }
        if (apkFile == null) {
            resp.status = HttpServletResponse.SC_NOT_FOUND
            return
        }

        try {
            prepareAPKForReevaluation(hash, subFolder, apkFile)
            val file = File("$uploadsFolder$subFolder/$hash.bin")
            // no exceptions so far
            val reporter = req.getParameter("reporter")
            resp.writer.println(
                gson.toJson(
                    mapOf(
                        "success" to "Re-evaluation request received.",
                        "hash" to hash,
                        "reporter" to reporter
                    )
                )
            )
            thread {
                try {
                    processNewFile(file, hash, reporter, revision, if (isPublic) null else apiKey)
                } catch (e: FileRejectedException) {
                    // something was not right. Cleanup and keep supposed apk and message for analysis
                    logger.warn(e.message)
                    e.folder?.deleteRecursively()
                    e.file.delete()
                    hashesInProcess.remove(hash)
                }
            }
        } catch (e: FileRejectedException) {
            resp.status = HttpServletResponse.SC_BAD_REQUEST
            gson.toJson(
                WSError("We already know this APK.", e.message)
            )
        }
    }

    /**
     * Checks for prior result. only used for public tests without public key.
     * @param file to remove or move to appropriate folder when there is a prior result available
     * @param hash to check for any existing result
     */
    private fun checkPriorResult(file: File, hash: String) {
        val match = StateHelper.findStateByHash(hash)
        if (match != null) {
            hashesInProcess.remove(hash)
            if (match.keys.first() == "public") {
                file.delete()
            } else {
                val state = match[match.keys.first()]!!
                val result = state.knownBinaries[hash]!!
                if (result.reproducible) {
                    moveFile(file, "$appsFolder/${state.appId}_${result.versionCode}_$hash.apk")
                } else {
                    moveFile(file, "$notReproducibleFolder/${state.appId}_${result.versionCode}_$hash.apk")
                }
                StateHelper.saveState(state.appId, hash, null, result)
            }
            val matchUrl = "http://$ip:8080/server/apks?appId=${match[match.keys.first()]!!.appId}&appHash=$hash"
            throw FileRejectedException("File already known: $matchUrl", file)
        }
    }

    private fun storeFileUpload(req: HttpServletRequest, subFolder: String): String {
        // create a random file name to store it to
        val nonceFile = File("$uploadsFolder$subFolder/${UUID.randomUUID()}.bin")

        req.inputStream.use { input -> nonceFile.outputStream().use { input.copyTo(it) } }
        val hash = sha256(nonceFile)
        if (hashesInProcess.contains(hash)) {
            nonceFile.delete()
            throw FileRejectedException("This File is already being processed.", nonceFile)
        }
        hashesInProcess[hash] = System.currentTimeMillis()
        val file = File("$uploadsFolder$subFolder/$hash.bin")
        if (file.isFile) {
            nonceFile.delete()
        } else {
            nonceFile.renameTo(file)
        }
        return hash
    }

    private fun prepareAPKForReevaluation(hash: String, subFolder: String, apk: File) {
        if (hashesInProcess.contains(hash)) {
            throw FileRejectedException("This File is already being processed.", apk)
        }
        hashesInProcess[hash] = System.currentTimeMillis()
        moveFile(apk, "$uploadsFolder$subFolder/$hash.bin")
    }

    @Synchronized
    private fun processNewFile(tmpFile: File, hash: String, reporter: String?, revision: String?, apiKey: String?) {
        val subFolder = if (apiKey.isNullOrBlank()) "" else "/$apiKey"
        val tmpFolder = File("$uploadsFolder$subFolder/$hash")
        unwrap(tmpFile, tmpFolder)

        // analyze
        val appId = xmlMapper
            .readValue(File("$tmpFolder/AndroidManifest.xml"), AndroidManifest::class.java)
            .appId
        val versionInfo = yamlMapper
                .readValue(File("$tmpFolder/apktool.yml"), ApkTool::class.java)
                .versionInfo

        var signatureVerified = false
        var reproducible = false
        var result = ""
        try {
            if (apiKey.isNullOrBlank()) {
                checkSignature(tmpFile, tmpFolder, appId)
            }
            signatureVerified = true
            result = checkReproducibility(tmpFile, tmpFolder, revision, appId)
            reproducible = true
            // cleanup
            hashesInProcess.remove(hash)
            tmpFolder.deleteRecursively()
            moveFile(tmpFile, "$appsFolder$subFolder/${appId}_${versionInfo.versionCode}_$hash.apk")
            if (!apiKey.isNullOrBlank()) {
                val oldResultFile = File("$notReproducibleFolder$subFolder/${appId}_${versionInfo.versionCode}_$hash.apk")
                if (oldResultFile.isFile) {
                    oldResultFile.delete()
                }
            }
        } catch (e: FileRejectedException) {
            result = e.message!!
            moveFile(tmpFile, "$notReproducibleFolder$subFolder/${appId}_${versionInfo.versionCode}_$hash.apk")
            if (!apiKey.isNullOrBlank()) {
                val oldResultFile = File("$appsFolder$subFolder/${appId}_${versionInfo.versionCode}_$hash.apk")
                if (oldResultFile.isFile) {
                    oldResultFile.delete()
                }
            }
            throw e
        } finally {
            StateHelper.saveState(appId, hash, apiKey, WSApplicationState(
                Date(),
                versionInfo.versionName,
                versionInfo.versionCode,
                reporter,
                reproducible,
                signatureVerified,
                result
            ))
        }
    }

    private fun unwrap(file: File, apkToolFolder: File) {
        val apkToolProcess = ProcessBuilder(
            "apktool", "d",
            "--output", "$apkToolFolder",
            "--frame-path", "$apktoolFrameworkPath",
            "$file")
            .inheritIO()
            .directory(file.parentFile)
            .start()
        // with 3 uploads processed in parallel, this timeout was triggered for one of the processes when set
        // to 1 minute. Now @Synchronized should prevent parallel execution and 3 minutes should be
        // a good compromise between zip-bomb protection and enough time to unzip.
        // TODO: detect zip bombs. time limit is not the best approach
        if (!apkToolProcess.waitFor(3, TimeUnit.MINUTES)) {
            // if apktool runs that long, something is shady. Might be a zip bomb. Kill it with fire.
            apkToolProcess.destroyForcibly()
            throw FileRejectedException("apktool timed out!", file, apkToolFolder)
        }
        if (!File("$apkToolFolder/AndroidManifest.xml").exists()) {
            // not an APK. Keeping uploaded file around for analysis, deleting the rest.
            throw FileRejectedException("File appears to not be an APK!", file, apkToolFolder)
        }
    }

    private fun checkSignature(file: File, apkToolFolder: File, appId: String) {
        val process = ProcessBuilder("/bin/sh", "-c",
            "${servletContext.getInitParameter("apksigner")} verify --print-certs $file | grep \"Signer #1 certificate SHA-256\" | awk '{print \$6}'")
            .start()
        // give the process reasonable time to finish
        // 2 seconds with parallel execution was not enough. Now with @Synchronized and 10s it
        // should be ok for most systems.
        val success = process.waitFor(10, TimeUnit.SECONDS)
        val signer = if (success) {
            process.inputStream.bufferedReader().readText().trim()
        } else {
            process.destroyForcibly()
            throw FileRejectedException("apksigner timed out!", file, apkToolFolder)
        }
        if (signer.isEmpty()) {
            // is apksigner installed properly and on the path?
            // might need to install android sdk build tools and something like
            // ln -s /opt/android-sdk/build-tools/28.0.3/apksigner /usr/bin/apksigner
            throw FileRejectedException("Something went wrong detecting the signature.", file, apkToolFolder)
        }
        if (StateHelper.getKnownSigners(appId)?.any { it.signer == signer } != true) {
            throw FileRejectedException("Unexpected signer $signer!", file, apkToolFolder)
        }
    }

    private fun checkReproducibility(file: File, apkToolFolder: File, revision: String?, appId: String): String {
        logger.debug("[$appId] Running test script... (It may take too long)")

        val revisionOverrideArg = if (revision != null) {
            "--revision-override $revision"
        } else {
            ""
        }

        val process = ProcessBuilder("/bin/sh", "-c",
            "${servletContext.getInitParameter("testScript")} --apk $file --not-interactive -c $revisionOverrideArg")
            .redirectErrorStream(true)
            .start()

        var line: String?
        var result = ""
        var readingResult = false
        val reader = process.inputStream.bufferedReader()
        val maxMillis = TimeUnit.MINUTES.toMillis(90)

        runBlocking {
            try {
                withTimeout(maxMillis) {
                    // TODO: 7/27/21 Mohammad - We can remove this while and just use a sed to
                    //  get the results but for now leave it as is for debug purposes
                    while (reader.readLine().also { line = it } != null) {
                        if (line!!.startsWith("===== Begin Results =====") || readingResult) {
                            result = result + line + "\n"
                            readingResult = !line!!.startsWith("===== End Results =====")
                        } else {
                            logger.debug("[$appId] $line")
                        }
                        yield()
                    }
                }
            } catch (e: TimeoutCancellationException) {
                process.destroyForcibly()
                throw FileRejectedException("Reproducibility check timed out!", file, apkToolFolder)
            }
        }

        logger.debug("[$appId] $result")
        if (result.isEmpty()) {
            // is test.sh path set properly in configs?
            throw FileRejectedException("Something went wrong while trying to reproduce the apk from wallet repository.", file, apkToolFolder)
        }
        // Examine verdict
        if (!result.contains("verdict:        reproducible")) {
            throw FileRejectedException(result, file, apkToolFolder)
        }

        return result
    }

    private fun sha256(file: File): String {
        val buffer = ByteArray(8 * 1024)
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        file.inputStream().use { inputStream ->
            var numRead = 0
            while (numRead != -1) {
                numRead = inputStream.read(buffer)
                if (numRead > 0) {
                    digest.update(buffer, 0, numRead)
                }
            }
        }
        return digest.digest().joinToString("") { "%02x".format(it) }
    }

    private fun moveFile(file: File, destinationPath: String) {
        val destinationFile = File(destinationPath)
        if (destinationFile.isFile) {
            destinationFile.delete()
        }
        if (!file.renameTo(destinationFile)) {
            logger.warn("Failed to move $file to $destinationFile.")
        }
    }

    companion object {
        private const val APP_JSON = "application/json"
        private val NO_DATA_ERROR = WSError("No data available", null)
    }
}


/// to parse the AndroidManifest.xml files extracted from APKs
@JacksonXmlRootElement(localName = "manifest")
@JsonIgnoreProperties(ignoreUnknown = true)
data class AndroidManifest(
    @JsonProperty("package")
    val appId: String
)

/// to parse the apktool.yml
@JsonIgnoreProperties(ignoreUnknown = true)
data class ApkTool(
    val versionInfo: ApkVersionInfo
)

data class ApkVersionInfo(
    val versionName: String,
    val versionCode: Long
)

class FileRejectedException(
    message: String,
    val file: File,
    val folder: File? = null
): Exception(message)