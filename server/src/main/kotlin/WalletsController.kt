package com.walletscrutiny.server

import com.google.gson.GsonBuilder
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet("/wallets")
class WalletsController : HttpServlet() {
    private val gson = GsonBuilder().disableHtmlEscaping().setDateFormat("yyyy-MM-dd hh:mm:ss").create()

    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        res.writer.write(gson.toJson(StateHelper.getSupportedWallets()))
        return
    }
}
