package com.walletscrutiny.server

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.walletscrutiny.libdataexchange.WSApplicationState
import com.walletscrutiny.libdataexchange.Wallet
import java.io.File
import java.io.FileNotFoundException

object StateHelper {
    private lateinit var state: State
    private var devStates: MutableMap<String, State> = mutableMapOf()
    private lateinit var knownSigners: Map<String, List<KnownSigner>>
    private lateinit var apiKeys: Map<String, ApiKey>
    private val yamlMapper = ObjectMapper(YAMLFactory()).also { it.registerKotlinModule() }
    private lateinit var dataFolder: File

    fun loadState(dataFolder: File) {
        this.dataFolder = dataFolder

        state = try {
            yamlMapper.readValue(File("$dataFolder/state.yaml"), State::class.java)
        } catch (e: FileNotFoundException) {
            State(0, mutableMapOf())
        }
        apiKeys = try {
            yamlMapper.readValue(
                File("$dataFolder/api-keys.yaml"),
                object : TypeReference<Map<String, ApiKey>>() {})
        } catch (e: FileNotFoundException) {
            mapOf()
        }
        apiKeys.keys.forEach {
            devStates[it] = try {
                yamlMapper.readValue(File("$dataFolder/$it-state.yaml"), State::class.java)
            } catch (e: FileNotFoundException) {
                State(0, mutableMapOf())
            }
        }
        knownSigners = yamlMapper.readValue(javaClass.getResource("/known-signers.yaml"),
            object : TypeReference<Map<String, List<KnownSigner>>>() {})
    }

    fun checkAPIKeyValidity(apiKey: String) = apiKeys[apiKey] != null

    fun getKnownSigners(appId: String) = knownSigners[appId]

    fun getAPIKeys() = apiKeys.keys

    fun getAPIKeyAccess(apiKey: String?) = apiKeys[apiKey]?.isAdmin ?: false

    fun getAppIdForHashInPublicState(hash: String): String? {
        for (appId in state.apps.keys) {
            if (state.apps[appId]?.knownBinaries!!.keys.contains(hash)) {
                return appId
            }
        }
        return null
    }

    fun getState(apiKey: String? = null): State {
        return if (apiKey.isNullOrBlank()) state else devStates[apiKey]!!
    }

    fun findStateByHash(hash: String): Map<String, WSApplication>? {
        var foundState = state.apps.values.firstOrNull { it.knownBinaries[hash] != null }
        if (foundState != null) {
            return mapOf("public" to foundState)
        }
        for (devState in devStates) {
            foundState = devState.value.apps.values.firstOrNull { it.knownBinaries[hash] != null }
            if (foundState != null) {
                return mapOf(devState.key to foundState)
            }
        }
        return null
    }

    fun saveState(appId: String, hash: String, apiKey: String?, wsApplicationState: WSApplicationState) {
        val stateFilePrefix = if (apiKey.isNullOrBlank()) "" else "$apiKey-"
        val targetState = getState(apiKey)
        targetState.apps[appId] =
            (targetState.apps[appId] ?: WSApplication(appId, mutableMapOf()))
                .also {
                    it.knownBinaries[hash] = wsApplicationState
                }
        yamlMapper.writeValue(File("$dataFolder/${stateFilePrefix}state.yaml"), targetState)
    }

    fun getSupportedWallets() = knownSigners.map { Wallet(it.key, it.value[0].name) }
}

/// our "database"
data class State(
    /**
     * To migrate to a new structure, the version is stored
     */
    val version: Int,
    /**
     * A Map from an appId to an WSApplication
     */
    val apps: MutableMap<String, WSApplication>
)

/**
 * All the information we have about a monitored Application
 */
data class WSApplication(
    val appId: String,
    /**
     * A Map from the hash of an APK to its WSApplicationState
     */
    val knownBinaries: MutableMap<String, WSApplicationState>
)

data class KnownSigner(
    val channel: String,
    val signer: String,
    val name: String
)

data class ApiKey(
    val name: String,
    val date: String,
    val isAdmin: Boolean
)