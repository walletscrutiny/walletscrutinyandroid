class ResultsPanel {
    errorMessageElement = document.getElementById('errorMessage')
    infoMessageElement = document.getElementById('infoMessage')
    buttonElement = document.getElementById('getResultsButton')
    tableElement = document.getElementById('table')
    resultModalElement = document.getElementById('resultModal')
    resultModalContentElement = document.getElementById('resultModalContent')
    publicResultsCheckbox = document.getElementById('publicResultsCheckbox')
    showingPublicResults = false
    results = {}
    isAdmin = false
    apiKey = null

    getResults(e) {
        e.preventDefault()
        this.hideElement(this.errorMessageElement)
        this.hideElement(this.infoMessageElement)
        this.hideElement(this.tableElement)
        this.results = {}
        this.showingPublicResults = this.publicResultsCheckbox.checked
        this.apiKey = document.getElementById('apiKey').value.trim()

        if (!this.showingPublicResults && (!this.apiKey || (this.apiKey === ''))) {
            this.showInfo('You need an API key to get private results! Showing public results instead!')
            this.publicResultsCheckbox.checked = true
            this.showingPublicResults = true
        }

        this.lockButton()

        fetch('/admin?action=apiKeyAccess&apiKey=' + this.apiKey, {
          method: 'GET',
          headers: { 'Accept': 'application/json' }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            if (res) {
                this.isAdmin = res.isAdmin
                const appId = document.getElementById('appId').value
                let queryString = '?appId=' + appId

                if (!this.showingPublicResults) {
                    queryString = queryString + '&apiKey=' + this.apiKey
                }

                return fetch('/apks' + queryString, {
                  method: 'GET',
                  headers: { 'Accept': 'application/json' }
                })
                .then(res => {
                    console.log(res)
                    switch (res.status) {
                        case 401:
                            this.showError('API key is not valid!')
                            return
                        case 404:
                            this.showError('No results found!')
                            return
                    }

                    return res.json()
                })
                .then(res => {
                    if (res) {
                        console.log(res)
                        this.results = res
                        this.fillTableContent()
                        this.showElement(this.tableElement)
                    }
                })
                .catch(error => this.showError('Something went wrong! Take a look at console for more info.'))
            }
        })
        .catch(error => this.showError('Something went wrong! Take a look at console for more info.'))
        .finally(() => this.unlockButton())
    }

    fillTableContent() {
        const tableBody = document.getElementById('tableBody')
        tableBody.innerHTML = ''
        let rows = ''
        for (let hash in this.results) {
            rows = rows
                + `<tr onclick="resultsPanel.showTestDetails('${hash}')">
                     <td>${hash.slice(0, 8)}...</td>
                     <td>${this.results[hash].versionName}</td>
                     <td>${this.results[hash].versionCode}</td>
                     <td>${this.results[hash].date}</td>
                     <td>${this.results[hash].signatureVerified}</td>
                     <td>${this.results[hash].reproducible}</td>
                  </tr>`
        }
        tableBody.insertAdjacentHTML('afterbegin', rows)
    }

    showTestDetails(hash) {
        this.resultModalContentElement.innerHTML = ''
        const apiKeyParameter = (!this.showingPublicResults && this.apiKey && (this.apiKey.length > 0)) ? '&apiKey=' + this.apiKey : ''
        let markAsReproducibleElement = ''
        let requestReevaluationElement = ''
        if (!this.results[hash].reproducible && (this.isAdmin || (!this.showingPublicResults && this.apiKey && (this.apiKey.length > 0)))) {
            requestReevaluationElement =
                `<hr>
                 <div class="field">
                   <label class="label">Reporter (optional)</label>
                   <div class="control"><input class="input" type="text" placeholder="e.g Leo" id="reporter"></div>
                 </div>
                 <div class="field">
                   <label class="label">Revision (optional)</label>
                   <div class="control"><input class="input" type="text" placeholder="e.g. e3c414ca" id="revision"></div>
                 </div>
                 <div class="notification is-danger is-hidden" id="reevaluateErrorMessage"></div>
                 <div class="notification is-success is-hidden" id="reevaluateSuccessMessage"></div>
                 <button class="button is-success is-fullwidth mt-4" onclick="resultsPanel.requestReevaluation('${hash}')">Request Re-Evaluation</button>`
        }

        if (this.isAdmin && !this.results[hash].reproducible) {
            markAsReproducibleElement =
                `<div class="notification is-danger is-hidden" id="markAsReproducibleErrorMessage"></div>
                 <button class="button is-success is-fullwidth mt-4" onclick="resultsPanel.markAsReproducible('${hash}')">Mark As Reproducible</button>`
        }

        let content =
            `<div><span><strong>Hash: </strong></span>${hash}</div>
             <div><span><strong>Version Name: </strong></span>${this.results[hash].versionName}</div>
             <div><span><strong>Version Code: </strong></span>${this.results[hash].versionCode}</div>
             <div><span><strong>Date: </strong></span>${this.results[hash].date}</div>
             <div><span><strong>Signature Verified: </strong></span>${this.results[hash].signatureVerified}</div>
             <div><span><strong>Reproducible: </strong></span>${this.results[hash].reproducible}</div>
             <div><span><strong>Test Result: </strong></span><div><code class="pre-wrap">${this.results[hash].testResult}</code></div></div>
             <a class="button is-info is-fullwidth mt-4" target="_blank" href="/download-apk?appHash=${hash + apiKeyParameter}">Download APK</a>
             ${markAsReproducibleElement}
             ${requestReevaluationElement}`

        this.resultModalContentElement.insertAdjacentHTML('afterbegin', content)
        if (!this.resultModalElement.className.includes('is-active')) {
            this.resultModalElement.className += ' is-active'
        }
    }

    requestReevaluation(hash) {
        this.hideElement(document.getElementById('reevaluateErrorMessage'))
        this.hideElement(document.getElementById('reevaluateSuccessMessage'))
        let queryString = '?appHash=' + hash + '&apiKey=' + this.apiKey + '&isPublic=' + this.showingPublicResults
        const reporter = document.getElementById('reporter').value
        const revision = document.getElementById('revision').value
        if (reporter) {
            queryString = queryString + '&reporter=' + reporter
        }
        if (revision) {
            queryString = queryString + '&revision=' + revision
        }

        fetch('/apks' + queryString, {
          method: 'PUT',
          headers: { 'Accept': 'application/json' }
        })
        .then(res => {
            console.log(res)
            switch (res.status) {
                case 401:
                    this.showReevaluateErrorMessage('API key is not valid!')
                    return
                case 404:
                    this.showReevaluateErrorMessage('The APK doesn\'t exist on server or it\'s already being processed.')
                    return
                case 400:
                    const responseObj = res.json()
                    this.showReevaluateErrorMessage(responseObj.error + '\n' + responseObj.message)
                    return
            }

            this.showReevaluateSuccessMessage(
                'Thank you ' + reporter + ' for your request. We will shortly analyze it. You will find the result at https://backend.walletscrutiny/apks?appId=___your_app_id_here___'
            )
        })
        .catch(error => this.showError('Something went wrong! Take a look at console for more info.'))
        .finally(() => this.unlockButton())
    }

    markAsReproducible(hash) {
        this.hideElement(document.getElementById('markAsReproducibleErrorMessage'))
        let queryString = '&appHash=' + hash + '&apiKey=' + this.apiKey

        fetch('/admin?action=markAsReproducible' + queryString, {
          method: 'PUT',
          headers: { 'Accept': 'application/json' }
        })
        .then(res => {
            console.log(res)
            switch (res.status) {
                case 401:
                    this.showMarkAsReproducibleErrorMessage('API key is not valid!')
                    return
                case 404:
                    this.showMarkAsReproducibleErrorMessage('The APK doesn\'t exist on server.')
                    return
                case 400:
                    this.showMarkAsReproducibleErrorMessage('The hash is not provided!')
                    return
            }

            this.results[hash].reproducible = true
            this.fillTableContent()
            this.showTestDetails(hash)
        })
        .catch(error => this.showError('Something went wrong! Take a look at console for more info.'))
        .finally(() => this.unlockButton())
    }

    closeModal() {
        this.resultModalElement.className = this.resultModalElement.className.replace(' is-active', '')
    }

    showError(message) {
        this.errorMessageElement.textContent = message
        this.errorMessageElement.className = this.errorMessageElement.className.replace(' is-hidden', '')
    }

    showInfo(message) {
        this.infoMessageElement.textContent = message
        this.infoMessageElement.className = this.infoMessageElement.className.replace(' is-hidden', '')
    }

    showReevaluateErrorMessage(message) {
        const element = document.getElementById('reevaluateErrorMessage')
        element.textContent = message
        this.showElement(element)
    }

    showReevaluateSuccessMessage(message) {
        const element = document.getElementById('reevaluateSuccessMessage')
        element.textContent = message
        this.showElement(element)
    }

    showMarkAsReproducibleErrorMessage(message) {
        const element = document.getElementById('markAsReproducibleErrorMessage')
        element.textContent = message
        this.showElement(element)
    }

    hideElement(element) {
        if (!element.className.includes('is-hidden')) {
            element.className = element.className + ' is-hidden'
        }
    }

    showElement(element) {
        element.className = element.className.replace(' is-hidden', '')
    }

    lockButton() {
        if (!this.buttonElement.disabled) {
            this.buttonElement.className = this.buttonElement.className + ' is-loading'
            this.buttonElement.disabled = true
        }
    }

    unlockButton() {
        this.buttonElement.className = this.buttonElement.className.replace(' is-loading', '')
        this.buttonElement.disabled = false
    }
}

window.resultsPanel = new ResultsPanel()