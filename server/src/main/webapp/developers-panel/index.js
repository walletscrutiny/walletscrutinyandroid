class DevelopersPanel {
    errorMessageElement = document.getElementById('errorMessage')
    successMessageElement = document.getElementById('successMessage')
    buttonElement = document.getElementById('submitButton')

    updateFileName(element) {
      if (element.files.length > 0) {
          const fileName = document.getElementById('fileName')
          fileName.textContent = element.files[0].name
      }
    }

    upload(e) {
        e.preventDefault()
        this.hideMessages()

        const apiKey = document.getElementById('apiKey').value.trim()
        if (!apiKey) {
            this.showMessage(this.errorMessageElement, 'Please enter your API key.')
            return
        }
        let queryString = '?apiKey=' + apiKey

        const apkFile = document.getElementById('apkFile').files[0]
        if (!apkFile) {
            this.showMessage(this.errorMessageElement, 'Please select your wallet APK.')
            return
        }

        const reporter = document.getElementById('reporter').value
        const revision = document.getElementById('revision').value
        if (reporter) {
            queryString = queryString + '&reporter=' + reporter
        }
        if (revision) {
            queryString = queryString + '&revision=' + revision
        }

        this.lockButton()

        fetch('/apks' + queryString, {
          method: 'POST',
          body: apkFile,
          headers: {
                'Content-Type': 'application/binary',
                'Accept': 'application/json'
          }
        })
        .then(res => {
            console.log(res)
            if (res.status === 401) {
                this.showMessage(this.errorMessageElement, 'API key is not valid!')
                return
            }

            const responseObj = res.json()

            if (res.status === 400) {
                this.showMessage(this.errorMessageElement, responseObj.error + '\n' + responseObj.message)
                return
            }

            this.showMessage(
                this.successMessageElement,
                'Thank you ' + reporter + ' for providing this new file. We will shortly analyze it. You will find the result at https://backend.walletscrutiny/apks?appId=___your_app_id_here___'
            )
        })
        .catch(error => this.showMessage(this.errorMessageElement, 'Something went wrong! Take a look at console for more info.'))
        .finally(() => this.unlockButton())
    }

    showMessage(element, message) {
        element.textContent = message
        element.className = element.className.replace(' is-hidden', '')
    }

    hideMessages() {
        if (!this.errorMessageElement.className.includes('is-hidden')) {
            this.errorMessageElement.className = this.errorMessageElement.className + ' is-hidden'
        }

        if (!this.successMessageElement.className.includes('is-hidden')) {
            this.successMessageElement.className = this.successMessageElement.className + ' is-hidden'
        }
    }

    lockButton() {
        if (!this.buttonElement.disabled) {
            this.buttonElement.className = this.buttonElement.className + ' is-loading'
            this.buttonElement.disabled = true
        }
    }

    unlockButton() {
        this.buttonElement.className = this.buttonElement.className.replace(' is-loading', '')
        this.buttonElement.disabled = false
    }
}

window.developersPanel = new DevelopersPanel()