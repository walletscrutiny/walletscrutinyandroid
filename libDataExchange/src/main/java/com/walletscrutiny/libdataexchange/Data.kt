package com.walletscrutiny.libdataexchange

import java.util.*

/**
 * The state of an application binary (APK)
 */
data class WSApplicationState(
    /**
     * The date this APK was first seen
     */
    val date: Date,
    val versionName: String,
    val versionCode: Long,
    /**
     * The app might create a userID. This might be opt-in to not raise privacy concerns but the
     * idea is some kind of gamification, where users get an incentive to be the first to report new
     * binaries.
     */
    val reporterId: String?,
    val reproducible: Boolean,
    val signatureVerified: Boolean,
    val testResult: String
)

open class Wallet(
    val appId: String,
    val name: String
)

/**
 * Error responses
 */
data class WSError(
    val error: String,
    val message: String?
)
