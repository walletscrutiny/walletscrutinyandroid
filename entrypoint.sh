#!/bin/bash

cleanup() {
    echo "Performing cleanup..."
    podman rm -f -v $(podman ps -aq)
}

cleanup

git -C /ws-com pull

if [ "$ENV" == "dev" ]; then
  #./gradlew -t installDist &
  #./gradlew run --debug-jvm
  ./gradlew --project-cache-dir ./.docker-gradle appRunWar
else
  ./gradlew --project-cache-dir ./.docker-gradle appRunWar
fi
