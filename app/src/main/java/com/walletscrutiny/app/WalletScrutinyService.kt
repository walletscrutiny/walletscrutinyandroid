package com.walletscrutiny.app

import com.walletscrutiny.libdataexchange.WSApplicationState
import com.walletscrutiny.libdataexchange.Wallet
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface WalletScrutinyService {
    @Headers("accept: application/json")
    @GET("apks")
    fun getApkInfo(@Query("appId") appId: String, @Query("appHash") hash: String): Call<WSApplicationState>

    @Headers("accept: application/json")
    @GET("apks")
    fun checkForUpdates(@Query("appId") appId: String, @Query("versionCode") versionCode: Long): Call<List<WSApplicationState>>

    @Headers("accept: application/json")
    @POST("apks")
    fun upload(@Body apk: RequestBody,
               @Query("reporter") reporter: String
    ): Call<Map<String, String>>

    @Headers("accept: application/json")
    @GET("wallets")
    fun getSupportedWallets(): Call<List<Wallet>>
}