package com.walletscrutiny.app

object Constants {
    const val baseUrl = "https://backend.walletscrutiny.com/"

    // Shared preference prefixes
    const val THEY_CAN_REPRODUCE = "They_can_reproduce_"
    const val THEY_CANNOT_REPRODUCE = "They_cannot_reproduce_"
    const val THEY_CANNOT_VERIFY_SIGNATURE = "They_cannot_verify_signature_"
}