package com.walletscrutiny.app

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.work.*
import com.walletscrutiny.app.models.WalletInfo
import java.io.File
import java.io.FileInputStream
import java.math.BigInteger
import java.security.*
import java.security.spec.ECGenParameterSpec
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.concurrent.TimeUnit

object Utils {
    val TAG = "Utils"

    fun testCrypto() {
        val pair = getRandomKeyPair()
        val ecdsaSign: Signature = Signature.getInstance("SHA256withECDSA")
        ecdsaSign.initSign(pair.private)
        val msg = "text ecdsa with sha256".toByteArray(Charsets.UTF_8)
        ecdsaSign.update(msg)
        val signature: ByteArray = ecdsaSign.sign()
        Log.d(TAG, "Signature is ${BigInteger(1, signature).toString(16)}")
        ecdsaSign.initVerify(pair.public)
        ecdsaSign.update(msg)
        Log.d(TAG, if (ecdsaSign.verify(signature)) "valid" else "invalid")
    }

    fun getRandomKeyPair(): KeyPair = KeyPairGenerator.getInstance("EC").run {
        initialize(ECGenParameterSpec("secp256r1"))
        generateKeyPair()
    }

    fun loadKeyPair(publicKeyBytes: ByteArray, privateKeyBytes: ByteArray): KeyPair? = try {
        KeyFactory.getInstance("EC").run {
            val publicKey = generatePublic(X509EncodedKeySpec(publicKeyBytes))
            val privateKey = generatePrivate(PKCS8EncodedKeySpec(privateKeyBytes))
            KeyPair(publicKey, privateKey)
        }
    } catch (e: Exception) {
        Log.e(TAG, "Failed to load keyPair from ($publicKeyBytes, $privateKeyBytes)", e)
        null
    }

    fun startUpdateCheckWorker(
        context: Context,
        progressListener: ((Boolean) -> Unit)? = null,
        lifecycleOwner: LifecycleOwner? = null) {
        val work = PeriodicWorkRequestBuilder<UpdateCheckWorker>(30, TimeUnit.MINUTES)
            .setConstraints(
                Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .setRequiresBatteryNotLow(true)
                .build())
            .build()
        WorkManager
            .getInstance(context)
            .enqueueUniquePeriodicWork("UpdateChecker", ExistingPeriodicWorkPolicy.REPLACE, work)

        if ((progressListener != null) && (lifecycleOwner != null)) {
            WorkManager.getInstance(context)
                .getWorkInfoByIdLiveData(work.id)
                .observe(lifecycleOwner, Observer { workInfo ->
                    if (workInfo != null) {
                        if (workInfo.state == WorkInfo.State.ENQUEUED) {
                            progressListener(false)
                        } else if (workInfo.state == WorkInfo.State.RUNNING) {
                            progressListener(true)
                        }
                    }
                })
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannel(context: Context, channelId: String, name: String, description: String, importance: Int) {
        val channel = NotificationChannel(channelId, name, importance)
        channel.description = description
        channel.setShowBadge(false)
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
                .createNotificationChannel(channel)
    }

    fun showNotification(context: Context, channelId: String, notificationId: Int, hasProgress: Boolean, title: String, text: String? = null, link: String? = null) {
        val builder = NotificationCompat.Builder(context, channelId).apply {
            setContentTitle(title)
            setSmallIcon(R.drawable.small_icon)
            if (hasProgress) {
                setProgress(0, 0, true)
            }
            if (!text.isNullOrEmpty()) {
                setContentText(text)
                setStyle(NotificationCompat.BigTextStyle().bigText(text))
            }
            if (link != null) {
                val notificationIntent = Intent(Intent.ACTION_VIEW)
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                notificationIntent.data = Uri.parse(link)
                val pending = PendingIntent.getActivity(context, 0, notificationIntent, 0)
                setContentIntent(pending)
            }
        }
        NotificationManagerCompat.from(context)
                .notify(notificationId, builder.build())
    }

    fun hideNotification(context: Context, notificationId: Int) {
        NotificationManagerCompat.from(context).cancel(notificationId)
    }

    fun sha256(file: File): String {
        val inputStream = FileInputStream(file)
        val buffer = ByteArray(1024)
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        var numRead = 0
        while (numRead != -1) {
            numRead = inputStream.read(buffer)
            if (numRead > 0) {
                digest.update(buffer, 0, numRead)
            }
        }
        return digest.digest().joinToString("") { "%02x".format(it) }
    }

    fun getSupportedInstalledWallets(context: Context): ArrayList<WalletInfo> {
        val pkgManager = context.packageManager
        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pkgAppsList = pkgManager.queryIntentActivities(mainIntent, 0)
        val wallets = arrayListOf<WalletInfo>()
        for (app in WalletsHelper.getSupportedWallets()) {
            val info = pkgAppsList.find {
                app.appId == it.activityInfo.taskAffinity
            } ?: continue

            val wallet = WalletInfo(app).also {
                it.file = File(info.activityInfo.applicationInfo.publicSourceDir)
                it.hash = sha256(it.file!!)
                try {
                    it.image =  pkgManager.getApplicationIcon(app.appId)
                } catch (e: PackageManager.NameNotFoundException) {
                    e.printStackTrace()
                }
            }

            wallets.add(wallet)
        }
        return wallets
    }

    fun getAppVersionCode(context: Context, appId: String): Long {
        val packageInfo = context.packageManager.getPackageInfo(appId, 0)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            packageInfo.longVersionCode
        } else {
            packageInfo.versionCode.toLong()
        }
    }

    fun getAppVersion(context: Context, appId: String): String {
        val packageInfo = context.packageManager.getPackageInfo(appId, 0)
        return packageInfo.versionName
    }
}