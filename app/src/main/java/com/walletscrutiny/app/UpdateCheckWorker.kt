package com.walletscrutiny.app

import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.GsonBuilder
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import java.util.*

class UpdateCheckWorker(context: Context, wp: WorkerParameters): Worker(context, wp) {
    private val progressChannelId = "checking_wallets_channel"
    private val progressNotificationId = 0
    private val gson = GsonBuilder().disableHtmlEscaping().setDateFormat("yyyy-MM-dd hh:mm:ss").create()

    private val walletScrutinyService =  Retrofit.Builder()
        .baseUrl(Constants.baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(WalletScrutinyService::class.java)

    override fun doWork(): Result {
        createNotificationChannels()
        try {
            Utils.showNotification(applicationContext, progressChannelId, progressNotificationId,
                    true, applicationContext.getString(R.string.checking_in_progress))
            check()
        } finally {
            Utils.hideNotification(applicationContext, progressNotificationId)
        }
        return Result.success()
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Utils.createNotificationChannel(
                    applicationContext,
                    progressChannelId,
                    applicationContext.getString(R.string.checking_wallets_notification_name),
                    applicationContext.getString(R.string.checking_wallets_notification_desc),
                    NotificationManager.IMPORTANCE_LOW
            )

            for (app in WalletsHelper.getSupportedWallets()) {
                Utils.createNotificationChannel(
                        applicationContext,
                        app.appId,
                        app.appId,
                        String.format(applicationContext.getString(R.string.wallet_notification_desc), app.appId),
                        NotificationManager.IMPORTANCE_DEFAULT
                )
            }
        }
    }

    private fun check() {
        val startTime = System.currentTimeMillis()
        Log.d("UpdateCheckWorker", "checking start at ${Date()}")
        val context = applicationContext
        val preferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE)
        // Get installed packages
        val installedWallets = Utils.getSupportedInstalledWallets(context)
        for (app in installedWallets) {
            checkForUpdates(app.appId)

            // Don't check this apk if we already know about its reproducibility state.
            if (preferences.getBoolean("${Constants.THEY_CAN_REPRODUCE}${app.hash}", false)
                || preferences.getBoolean("${Constants.THEY_CANNOT_REPRODUCE}${app.hash}", false)) {
                continue
            }
            // Check if the server already knows this apk, then update the local state.
            val state = walletScrutinyService.getApkInfo(app.appId, app.hash!!).execute().let {
                if (it.isSuccessful) {
                    val response = it.body()
                    if (response?.versionName != null) {
                        when {
                            // signature is verified the apk was reproducible
                            response.reproducible -> {
                                preferences.edit().putBoolean("${Constants.THEY_CAN_REPRODUCE}${app.hash}", true)
                                    .remove("${Constants.THEY_CANNOT_VERIFY_SIGNATURE}${app.hash}").apply()
                            }
                            // not reproducible but signature was verified
                            response.signatureVerified -> {
                                showNotReproducibleNotification(app.appId)
                                preferences.edit().putBoolean("${Constants.THEY_CANNOT_REPRODUCE}${app.hash}", true)
                                    .remove("${Constants.THEY_CANNOT_VERIFY_SIGNATURE}${app.hash}").apply()
                            }
                            // couldn't verify the signature (maybe something is wrong technically)
                            else -> {
                                preferences.edit().putBoolean("${Constants.THEY_CANNOT_VERIFY_SIGNATURE}${app.hash}", true).apply()
                            }
                        }
                    }
                    response
                } else {
                    null
                }
            }
            val message = when {
                preferences.getBoolean("${Constants.THEY_CAN_REPRODUCE}${app.hash}", false) -> {
                    "${app.appId} APK was reproducible: Reporter ${state!!.reporterId} sent it on ${state.date}."
                }
                preferences.getBoolean("${Constants.THEY_CANNOT_REPRODUCE}${app.hash}", false) -> {
                    "${app.appId} APK was not reproducible: Reporter ${state!!.reporterId} sent it on ${state.date}."
                }
                preferences.getBoolean("${Constants.THEY_CANNOT_VERIFY_SIGNATURE}${app.hash}", false) -> {
                    "Couldn't verify ${app.appId} APK signature: Reporter ${state!!.reporterId} sent it on ${state.date}."
                }
                // Server doesn't know the apk, So upload it.
                else -> {
                    val fileBody = RequestBody.create(MediaType.parse("application/binary"), app.file!!)
                    val reporter = preferences.getString("pubKey", "anonymous")!!
                    val result = try {
                        walletScrutinyService.upload(fileBody, reporter).execute().let {
                            if (it.isSuccessful) {
                                it.body()
                            } else {
                                mapOf("error" to it.errorBody()?.string())
                            }
                        }
                    } catch (ste: SocketTimeoutException) {
                        // if we fail to upload, we will try again later
                        mapOf("error" to "request timed out")
                    }
                    "You installed an APK not known to WalletScrutiny.com. Server replied $result."
                }
            }
            Log.d("UpdateCheckWorker", message)
        }
        Log.d("UpdateCheckWorker", "Check took ${System.currentTimeMillis() - startTime}ms.")
    }

    private fun checkForUpdates(appId: String) {
        val versionCode = Utils.getAppVersionCode(applicationContext, appId)
        // Check for updates
        val recentVersions = walletScrutinyService.checkForUpdates(appId, versionCode).execute().body()
        if (!recentVersions.isNullOrEmpty()) {
            Log.d("CheckForUpdates", "There are newer versions for $appId: $recentVersions")
            val hasNotReproducibleUpdate = recentVersions.any { !it.reproducible }
            val nextVersion = recentVersions[0]
            val preferences = applicationContext.getSharedPreferences("settings", Context.MODE_PRIVATE)
            // Only show a notification if there is a newer version than what we already know
            if (nextVersion.versionCode > preferences.getLong("Latest_known_version_$appId", 0)) {
                val message = if (hasNotReproducibleUpdate) {
                    applicationContext.getString(R.string.not_reproducible_update_available_desc,
                        appId)
                } else {
                    applicationContext.getString(R.string.reproducible_update_available_desc, appId,
                        recentVersions.last().versionName)
                }
                Utils.showNotification(
                    applicationContext,
                    appId,
                    WalletsHelper.getSupportedWallets().indexOfFirst { it.appId == appId },
                    false,
                    applicationContext.getString(R.string.update_available),
                    message,
                    String.format(applicationContext.getString(R.string.ws_wallet_url), appId)
                )
                // Add this version to shared preferences to prevent showing this notification for the same version again
                preferences.edit().putLong("Latest_known_version_$appId", nextVersion.versionCode).apply()
            }
        } else {
            Log.d("CheckForUpdates", "$appId is up to date")
        }
    }

    private fun showNotReproducibleNotification(appId: String) {
        Utils.showNotification(
            applicationContext,
            appId,
            WalletsHelper.getSupportedWallets().indexOfFirst { it.appId == appId },
            false,
            applicationContext.getString(R.string.not_reproducible_apk, appId),
            applicationContext.getString(R.string.not_reproducible_apk_desc, appId),
            applicationContext.getString(R.string.ws_wallet_url, appId)
        )
    }
}