package com.walletscrutiny.app.models

import android.graphics.drawable.Drawable
import com.walletscrutiny.libdataexchange.Wallet
import java.io.File

class WalletInfo(
    appId: String,
    name: String,
    var image: Drawable? = null,
    var hash: String? = null,
    var file: File? = null
) : Wallet(appId, name) {
    constructor(wallet: Wallet) : this(
        wallet.appId,
        wallet.name
    )
}
