package com.walletscrutiny.app.ui.home

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.walletscrutiny.app.R

class WalletsAdapter(private val wallets: ArrayList<WalletItemModel>) :
    RecyclerView.Adapter<WalletsAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.wallet_name)
        val packageNameTextView: TextView = view.findViewById(R.id.wallet_package_name)
        val verdictTextView: TextView = view.findViewById(R.id.wallet_verdict)
        val walletImageView: ImageView = view.findViewById(R.id.wallet_image)
        val hasUpdateImageView: ImageView = view.findViewById(R.id.wallet_update)
        val versionTextView: TextView = view.findViewById(R.id.wallet_version)
        val hashTextView: TextView = view.findViewById(R.id.wallet_hash)

        init {
            view.setOnClickListener {
                val browserIntent = Intent(Intent.ACTION_VIEW)
                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                browserIntent.data = Uri.parse(view.context.getString(
                    R.string.ws_wallet_url, wallets[adapterPosition].appId
                ))
                view.context.startActivity(browserIntent)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.wallet_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val resources = viewHolder.itemView.resources

        viewHolder.walletImageView.setImageDrawable(wallets[position].image)
        viewHolder.nameTextView.text = wallets[position].name
        viewHolder.packageNameTextView.text = wallets[position].appId
        viewHolder.verdictTextView.text = wallets[position].verdict
        viewHolder.versionTextView.text = wallets[position].version
        viewHolder.hashTextView.text = wallets[position].hash

        viewHolder.verdictTextView.setTextColor(resources.getColor(
            when (wallets[position].verdict) {
                "Reproducible" -> R.color.reproducible
                "Not Reproducible" -> R.color.notReproducible
                else -> R.color.pending
            }
        ))

        if (wallets[position].hasUpdateAvailable) {
            viewHolder.hasUpdateImageView.visibility = View.VISIBLE
        }
    }

    override fun getItemCount() = wallets.size
}
