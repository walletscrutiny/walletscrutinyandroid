package com.walletscrutiny.app.ui.home

import android.graphics.drawable.Drawable
import com.walletscrutiny.app.models.WalletInfo

data class WalletItemModel(
    val appId: String,
    val name: String,
    val image: Drawable?,
    val verdict: String,
    val hasUpdateAvailable: Boolean,
    val version: String,
    val hash: String
) {
    constructor(
        wallet: WalletInfo,
        verdict: String,
        hasUpdateAvailable: Boolean,
        version: String,
        hash: String
    ) : this(
        wallet.appId,
        wallet.name,
        wallet.image,
        verdict,
        hasUpdateAvailable,
        version,
        hash
    )
}