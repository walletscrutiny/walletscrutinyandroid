package com.walletscrutiny.app

import android.util.Log
import com.google.gson.GsonBuilder
import com.walletscrutiny.libdataexchange.Wallet
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object WalletsHelper {
    private var supportedWallets: List<Wallet> = listOf()
    private var lastUpdateAt: Long = 0
    private const val MAXIMUM_VALID_TIME: Long = 1000 * 60 * 60 * 24 // 1 day
    private val gson = GsonBuilder().disableHtmlEscaping().setDateFormat("yyyy-MM-dd hh:mm:ss").create()

    private val walletScrutinyService =  Retrofit.Builder()
        .baseUrl(Constants.baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(WalletScrutinyService::class.java)

    private fun fetchSupportedWallets() {
        supportedWallets = walletScrutinyService.getSupportedWallets().execute().body() ?: listOf()
        lastUpdateAt = Date().time
    }

    fun getSupportedWallets(): List<Wallet> {
        if (supportedWallets.isEmpty() || (Date().time >= lastUpdateAt + MAXIMUM_VALID_TIME)) {
                fetchSupportedWallets()
        }
        Log.d("WalletsHelper", "supported wallets: ${supportedWallets.map { it.appId }}")
        return supportedWallets
    }
}