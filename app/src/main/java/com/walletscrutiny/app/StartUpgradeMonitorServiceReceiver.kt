package com.walletscrutiny.app

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


/**
 * Start check worker also after reboot.
 */
class StartUpgradeMonitorServiceReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != Intent.ACTION_BOOT_COMPLETED) {
            return
        }
        Utils.startUpdateCheckWorker(context.applicationContext)
    }
}
