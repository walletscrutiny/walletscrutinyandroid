package com.walletscrutiny.app

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Base64.*
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.walletscrutiny.app.ui.home.WalletItemModel
import com.walletscrutiny.app.ui.home.WalletsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.security.KeyPair
import kotlin.experimental.xor
import kotlin.math.min
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private val preferences by lazy { getSharedPreferences("settings", Context.MODE_PRIVATE) }
    private var keyPair: KeyPair? = null

    private val sharedPreferenceListener =
        OnSharedPreferenceChangeListener { sharedPreferences, key ->
            setWalletsListData()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Utils.startUpdateCheckWorker(applicationContext, { updateProgress(it) }, this)

        preferences.registerOnSharedPreferenceChangeListener(sharedPreferenceListener)
    }

    override fun onResume() {
        super.onResume()
        val publicKeyBytes = decode(preferences.getString("pubKey", ""), DEFAULT)
        val privateKeyBytes = decode(preferences.getString("privKey", ""), DEFAULT)
        keyPair = Utils.loadKeyPair(publicKeyBytes, privateKeyBytes)
        updateUI()
        setWalletsListData()
    }

    override fun onPause() {
        preferences.unregisterOnSharedPreferenceChangeListener(sharedPreferenceListener)
        super.onPause()
    }

    fun createNewIdentity(view: View) {
        keyPair = Utils.getRandomKeyPair().also {
            val pubKeyString = encodeToString(it.public.encoded, DEFAULT)
            val privKeyString = encodeToString(it.private.encoded, DEFAULT)
            preferences
                .edit()
                .putString("pubKey", pubKeyString)
                .putString("privKey", privKeyString)
                .apply()
        }
        updateUI()
    }

    private fun updateProgress(isInProgress: Boolean) {
        checkingProgressBarLayout.visibility = if (isInProgress) View.VISIBLE else View.GONE
    }

    private fun updateUI() {
        keyPair?.public?.encoded?.also {
            updateIdenticon(it)
        }
    }

    /**
     * The identicon should be recognizable but not too much code. It's a two-color 15x15 pixel
     * symmetric image. The public key bytes are used to pick two clear and different colors and
     * draw 8x15 pixels in either of those colors. 120 pixels and 8 bit for the color pair.
     */
    private fun updateIdenticon(pubKey: ByteArray) {
        val bm = Bitmap.createBitmap(15, 15, Bitmap.Config.ARGB_8888)
        val identicon = Canvas(bm)

        val entropy = pubKey.asList().chunked(16).reduce { acc, value ->
            val result = ArrayList(acc)
            for (i in 0 until min(16, value.size)) {
                result[i] = acc[i] xor value[i]
            }
            result
        }

        val colorByte = entropy.last().toInt()
        val hiColor = getDistinctColor(colorByte)
        val loColor = getDistinctColor(colorByte xor 0xC0)

        val pixelList = entropy.flatMap { byte ->
            (0 until 8).map {
                byte.toInt() shr it and 1 == 1
            }
        }
        identicon.drawColor(loColor)
        val paint = Paint()
        paint.color = hiColor
        (0 until 8).forEach { x ->
            (0 until 15).forEach { y ->
                if (pixelList[x+y*8]) {
                    identicon.drawPoint(x.toFloat(), y.toFloat(), paint)
                    identicon.drawPoint(14 - x.toFloat(), y.toFloat(), paint)
                }
            }
        }
        val bmd = BitmapDrawable(resources, bm)
        bmd.setAntiAlias(false)
        bmd.isFilterBitmap = false
        ivIdenticon.setImageDrawable(bmd)
        ivIdenticon.visibility = View.VISIBLE
    }

    private fun getDistinctColor(byte: Int): Int {
        // 2 bit value, 2 bit saturation, 4 bit hue
        val hueV = byte and 0xE
        val hue: Float = 360f * hueV / 16 // 0..360, 4 bit

        val saturationV = (byte shr 4) and 0x3
        val saturation = 0.25f + 0.75f * saturationV / 4 // 0..1, 2 bit, 0.25..1

        val valueV = (byte shr 6) and 0x3
        val value = 0.2f + 0.6f * valueV / 4 // 0..1, 2 bit, 0.2..0.8

        return Color.HSVToColor(arrayOf(hue, saturation, value).toFloatArray())
    }

    private fun setWalletsListData() {
        val context = this
        GlobalScope.launch {
            val installedWallets = Utils.getSupportedInstalledWallets(context)
            runOnUiThread {
                val installedWalletItems = arrayListOf<WalletItemModel>()
                for (app in installedWallets) {
                    val verdict = when {
                        preferences.getBoolean("${Constants.THEY_CAN_REPRODUCE}${app.hash}", false) -> "Reproducible"
                        preferences.getBoolean("${Constants.THEY_CANNOT_REPRODUCE}${app.hash}", false) -> "Not Reproducible"
                        else -> "Pending"
                    }
                    val hasUpdateAvailable =
                        preferences.getLong("Latest_known_version_${app.appId}", 0) > Utils.getAppVersionCode(context, app.appId)

                    val version = Utils.getAppVersion(context, app.appId)

                    installedWalletItems.add(WalletItemModel(app, verdict, hasUpdateAvailable, version, app.hash!!))
                }
                val walletsAdapter = WalletsAdapter(installedWalletItems)

                val walletsList: RecyclerView = findViewById(R.id.wallets_list)
                walletsList.adapter = walletsAdapter
            }
        }
    }
}
